FROM alpine
RUN apk update --no-cache \
  && apk upgrade --no-cache
RUN apk add --no-cache arpwatch \
  bind-tools \
  bmon \
  ca-certificates \
  curl \
  iftop \
  iperf3 \
  nmap \
  openssl \
  tcpdump \
  iptables \
  busybox-extras \
  openssh \
  nmap-ncat
ENTRYPOINT ["/bin/sh"]
