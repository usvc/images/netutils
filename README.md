# `usvc/images/netutils`

[![pipeline status](https://gitlab.com/usvc/images/netutils/badges/master/pipeline.svg)](https://gitlab.com/usvc/images/netutils/commits/master)
[![dockerhub link](https://img.shields.io/badge/dockerhub-usvc%2Fnetutils-blue)](https://hub.docker.com/r/usvc/netutils)


This is an image containing network debugging tools.

# Usage

## Pull the Image

```sh
docker pull usvc/netutils:latest
```

## Run the Image

```sh
docker run -it usvc/netutils:latest
```

# Development Runbook

## CI/CD Variables

| Key | Variable |
| --- | --- |
| `DOCKER_REGISTRY_URL` | URL for Docker registry |
| `DOCKER_REGISTRY_USERNAME` | Username for Docker registry |
| `DOCKER_REGISTRY_PASSWORD` | Password for Docker registry |
| `DEPLOY_KEY` | Deploy key in base64 |

## Generating the `DEPLOY_KEY`

Run:

```sh
make .ssh
```

Copy the contents of `./.ssh/id_rsa.b64` and paste it into the `DEPLOY_KEY` variable in the CI settings.

Copy the contents of `./.ssh/id_rsa.pub` and paste it as a Deploy Key in the repository settings.

# License

This code is licensed under [the MIT license](./LICENSE).
