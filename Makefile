IMAGE_URL=usvc/netutils
DATESTAMP=$$(date +'%Y%m%d')

build:
	@docker build -t $(IMAGE_URL):latest .

shell: build
	@docker run -it $(IMAGE_URL):latest

test: build
	@container-structure-test test \
	 	--verbosity debug \
	 	--image $(IMAGE_URL):latest \
		--config test.yaml

publish: build
	@docker tag $(IMAGE_URL):latest $(IMAGE_URL):$(DATESTAMP)
	@docker push $(IMAGE_URL):latest
	@docker push $(IMAGE_URL):$(DATESTAMP)

.ssh:
	@mkdir -p ./.ssh
	@ssh-keygen -t rsa -b 8192 -f ./.ssh/id_rsa -N ""
	@cat ./.ssh/id_rsa | base64 -w 0 > ./.ssh/id_rsa.b64
